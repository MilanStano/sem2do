import sun.misc.Compare;

import java.io.*;
import java.util.Comparator;
import java.util.Scanner;

/**
 * Created by mino on 20.4.2016.
 */
public class NacitajSubor{
    private String nazovSuboruA;//subor s hodnotami A
    private String nazovSuboruC;//subor s hodnotami C
    private Batoh batoh;//náš batoh :D :D

    /**
     * konštrukotr kotrý inicializuje nazov suobrov a(veľkost predmetov) a C cenup redmertov a vytovrý prázdny batoh
     * @param nazovSuboruA
     * @param nazovSuboruC
     * @param maxKapacitaBatohuK
     * @param maxPocetPrvkovR
     */
    public NacitajSubor(String nazovSuboruA, String nazovSuboruC,int maxKapacitaBatohuK, int maxPocetPrvkovR) {
        this.batoh = new Batoh(maxKapacitaBatohuK,maxPocetPrvkovR);
        this.nazovSuboruC = nazovSuboruC;
        this.nazovSuboruA = nazovSuboruA;
    }

    /**
     *
     * @throws FileNotFoundException
     * @throws java.io.IOException
     * Metoda ktorá načíta údaje z oboch dokumentoch a vloží ich do ArrayListu
     */
    public void CitajSubor() throws FileNotFoundException {
        Scanner scA = new Scanner(new File(nazovSuboruA));//hodnoty prvku zo suboru tj hmotnost prvku
        Scanner scC = new Scanner(new File(nazovSuboruC));//cena prvku


       try {

           while (scA.hasNext()) {//pokial existuje dalsi prvok v subore
                System.out.println(scA.nextInt()+" "+scC.nextInt());
               this.batoh.getBatoh().add(new Prvok(scA.nextInt(), scC.nextInt(), true));//vytvorime a vložíme do batohu novy prvok
           }
       }finally {
           scA.close();
           scC.close();

       }

    }




}
