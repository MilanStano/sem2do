import java.util.Comparator;

/**
 * Created by mino on 21.4.2016.
 */
public class Prvok {
    int hodnotaA;//hmotnost v batohu
    int getHodnotaB;//cena prvku
    boolean Zaradeny;//true zaradený do batohu, false nezaradený do batohu

    /**
     *
     * @param hodnotaA .---htmostno prvku v batohu
     * @param getHodnotaB---cena prvku  v batohu
     * @param zaradeny ---true zaradený do batohu, false nezaradený do batohu
     */
    public Prvok(int hodnotaA, int getHodnotaB, boolean zaradeny) {
        this.hodnotaA = hodnotaA;
        this.getHodnotaB = getHodnotaB;
        Zaradeny = zaradeny;
    }

    public int getHodnotaA() {
        return hodnotaA;
    }

    public int getGetHodnotaB() {
        return getHodnotaB;
    }

    public boolean isZaradeny() {

        return Zaradeny;
    }

    public void setZaradeny(boolean zaradeny) {
        Zaradeny = zaradeny;

    }



}
