import java.util.PriorityQueue;

/**
 * Created by mino on 21.4.2016.
 */
public class Batoh {
   int kapacitaK;
    int maxPocetPredmetov;
    PriorityQueue<Prvok> batoh;

    /**
     * Konštrukotr ktorý inicializuje kapacitu K a maximalný počet predmetov v batohu R, na začiatku inicializuje prázdny batoh----
     * @param kapacitaK//kapacita batohu
     * @param maxPocetPredmetov//maximalny pocet predmetov v batohu
     */
    public Batoh(int kapacitaK, int maxPocetPredmetov) {
        this.kapacitaK = kapacitaK;
        this.maxPocetPredmetov = maxPocetPredmetov;
        this.batoh=new PriorityQueue<Prvok>();
    }

    public PriorityQueue<Prvok> getBatoh() {

        return batoh;
    }

    public void setBatoh(PriorityQueue<Prvok> batoh) {
        this.batoh = batoh;
    }

    public int getKapacitaK() {
        return kapacitaK;
    }

    public int getMaxPocetPredmetov() {
        return maxPocetPredmetov;

    }






    @Override
    public String toString() {
        return "Batoh{" +
                "kapacitaK=" + kapacitaK +
                ", maxPocetPredmetov=" + maxPocetPredmetov +
                ", batoh=" + batoh +
                '}';
    }
}
